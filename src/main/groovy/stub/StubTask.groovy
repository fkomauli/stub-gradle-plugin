package stub

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

class StubTask extends DefaultTask {

    @TaskAction
    void stub() {
        def message = project.stub.message
        (1..project.stub.repeat).each {
            logger.lifecycle(message)
        }
    }
}
