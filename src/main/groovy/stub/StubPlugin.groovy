package stub

import org.gradle.api.Plugin
import org.gradle.api.Project

class StubPlugin implements Plugin<Project> {

    void apply(Project project) {
        project.extensions.create('stub', StubPluginExtension)
        project.task('stub', type: StubTask)
    }
}
