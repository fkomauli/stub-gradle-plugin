# Stub gradle plugin

A simple gradle plugin implementation example.

## Building

The plugin must be installed in the local repository before being used.
Clone and publish the plugin by running:

```
git clone https://fkomauli@bitbucket.org/fkomauli/stub-gradle-plugin.git
cd stub-gradle-plugin
gradle build publish
```

## Usage

Write a `build.gradle` file containing the following lines.

```
buildscript {
    repositories {
        mavenLocal()
        jcenter()
    }
    dependencies {
        classpath group: 'it.unimi.di', name: 'stub', version: '1.0-SNAPSHOT'
    }
}

apply plugin: 'it.unimi.di.stub'
```

Then check with the `gradle tasks` command that the plugin is correctly loaded.
The output should contain the lines

```
Other tasks
-----------
stub
```

To execute the plugin, run `gradle stub`.
The output will be similar to

```
:stub
Running stub task

BUILD SUCCESSFUL
```
